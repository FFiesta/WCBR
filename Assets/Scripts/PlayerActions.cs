﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerActions : MonoBehaviour {

	PlayerInput pi;
	Rigidbody2D rb;

	float action1Cooldown;
	float action2Cooldown;

	void Awake () {
		pi = GetComponent<PlayerInput>();
		rb = GetComponent<Rigidbody2D>();

	}


    public void Move(Vector2 direction, float speed)
    {
        rb.velocity = direction * speed * Time.deltaTime;
    }

    void Update () {

        
        /*
        
		// Load class' script.
		switch (ps.playerClass) {
			case PlayerStats.PlayerClass.Wizard: 

				if (GetComponent<Wizard>() != null)
					gameObject.AddComponent<Wizard>();
					
				var wiz = GetComponent<Wizard>();
				if(pi.action1)
					if (wiz.currentSpellbook[0] != null)
						wiz.CastSpell(wiz.currentSpellbook[0]);
					else
						Debug.Log("No spell assigned to spell number 1");
				if(pi.action2)
					if (wiz.currentSpellbook[1] != null)
						wiz.CastSpell(wiz.currentSpellbook[1]);
					else
						Debug.Log("No spell assigned to spell number 2");

				break;
				
			case PlayerStats.PlayerClass.Archer: /* Load archer settings *//* break;
		}
		Motion(pi.x, pi.y);

		if(pi.action1 && action1Cooldown <= 0) {
			a.SetTrigger("Attack");
			Debug.Log("Attacking");
			Projectile fireball = Instantiate(ps.fireball1);
			fireball.transform.position = ps.weaponPosition.position;
			action1Cooldown = ps.action1castSpeed;
			SetCastSpeedAnimation(ps.action1castSpeed);
		} else {
			a.ResetTrigger("Attack");
		}
		if (action1Cooldown >= 0)
			action1Cooldown -= Time.deltaTime;
		else 
			RestoreSpeedAnimation();
		

		
		if(pi.action2 && action2Cooldown <= 0) {
			a.SetTrigger("Attack");
			Debug.Log("Attacking");
			Projectile fireball = Instantiate(ps.fireball2);
			fireball.transform.position = ps.weaponPosition.position;
			action2Cooldown = ps.action2castSpeed;
			SetCastSpeedAnimation(ps.action2castSpeed);
		} else {
			a.ResetTrigger("Attack");
		}
		if (action2Cooldown >= 0)
			action2Cooldown -= Time.deltaTime;
		else 
			RestoreSpeedAnimation();
		*/
	}
    /*

	public void Motion(int x, int y) {
		rb.velocity = new Vector2(x,y) * ps.speed;
		a.SetFloat("x", x);
		a.SetFloat("y", y);

		if (x != 0 || y != 0){
			a.SetBool("Walking", true);
		} else {
			a.SetBool("Walking", false);
		}
	}

	public void SetCastSpeedAnimation(float actionCastSpeed) {
		a.speed = a.GetCurrentAnimatorClipInfo(0).Length / actionCastSpeed -1;
	}

	public void RestoreSpeedAnimation() {
		a.speed = 1;
	}
    */
}
