﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelfDestruct : MonoBehaviour {

	public float lifetime = 3;

	float currentTime;
	
	void Update () {
		if (currentTime >= lifetime)
			Destroy(this.gameObject);
		currentTime += Time.deltaTime;
	}
}
