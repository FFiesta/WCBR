﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/*
	Uses of PropagateGO:
		-Spell VFX (.
		-May work as a (re)spawner of enemies.
 */

public class PropagateGO : MonoBehaviour {

	public GameObject target;
	public float interval;

	public enum SelfDestructType {
		None,
		Basic
	} 

	public SelfDestructType selfDestructType;
	public float setSelfDestructTimer = 3;
	public float setFadeOutTimer = 3;

	public GameObject[] drops;


	void Update () {
		var instance = Instantiate(target);
		instance.transform.position = gameObject.transform.position;

		if (selfDestructType != SelfDestructType.None)
			instance.AddComponent<SelfDestruct>().lifetime = setSelfDestructTimer; 

		switch (selfDestructType) {
			case SelfDestructType.Basic: 
				
				break;
		}

		/*
			Other SelfDestruct types such as 
			-FadeOut... uhh.. and just FadeOut?

			Maybe it's unnecesary to use enum 
			in this case, only time will tell.
		*/
		
		
	}
}
