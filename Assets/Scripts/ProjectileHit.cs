﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileHit : MonoBehaviour {

	float currentTime;
	float lifetime;
	Animator anim;

	void Awake() {
		anim = GetComponent<Animator>();
		lifetime = anim.GetCurrentAnimatorStateInfo(0).length;
	}
	
	// Update is called once per frame
	void Update () {
		currentTime += Time.deltaTime;

		if(currentTime > lifetime)
			Destroy(this.gameObject);
	}
}
