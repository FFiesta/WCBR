﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HUD : MonoBehaviour {

    public Text healthNumber;
    public Image healthBar;


    public Text energyNumber;
    public Image energyBar;

    public int digitQuantity;

    public Stats target;

    void Update()
    {
        healthNumber.text = System.Math.Round(target.GetHealth(), digitQuantity) + " / " + System.Math.Round(target.maxHealthPoints, digitQuantity);
        healthBar.fillAmount = target.GetHealth() / target.maxHealthPoints;

        energyNumber.text = System.Math.Round(target.GetEnergy(), digitQuantity) + " / " + System.Math.Round(target.maxEnergyPoints, digitQuantity);
        energyBar.fillAmount = target.GetEnergy() / target.maxEnergyPoints;
    }
    

}
