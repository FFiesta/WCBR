﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpecialAbility : MonoBehaviour {

    Rigidbody2D rb;
    public AudioClip onLaunchSnd;
    public AudioClip onCollisionSnd;
    public SelfDestruct explosionObject;
    public int abilityLevel = 1;
    public enum DamageType
    {
        Normal
    }
    public DamageType damageType;

    void Awake() {
        rb = GetComponent<Rigidbody2D>();
    }

	void Update () {
		
	}
}
