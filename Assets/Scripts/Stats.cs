﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stats : MonoBehaviour {

	public float maxHealthPoints = 10;
	public float maxEnergyPoints = 0;
    public float startingHealthPoints = 10;
    public float startingEnergyPoints = 0;
    float curHealthPoints;
	float curEnergyPoints;
	float healthRegenFactor = 1;
	float energyRegenFactor = 20;
	public float poisonTime;
	public float poisonFactor;
	public float incinerationTime;
	public float incinerationFactor = 2;
    public float moveSpeed = 8;

	public GameObject moderatedlyDamagedSprite;
	public GameObject heavilyDamagedSprite;

	public enum Debuff {
		Incinerated,
		Poisoned,
		Stunned,
		ModeratedlyDamaged,
		HeavilyDamaged
	};

	List<Debuff> currentDebuffs;

	void Awake ()
    {
        curHealthPoints = startingHealthPoints;
        curEnergyPoints = startingEnergyPoints;
    }

	void Update () {
		// Debuff control.
		if (curHealthPoints <= 0 ) {
			if (FindDebuff(Debuff.Incinerated))
				curHealthPoints -= Time.deltaTime * incinerationFactor;

			if (FindDebuff(Debuff.Poisoned))
				curHealthPoints -= Time.deltaTime * poisonFactor;
		}

		// Health Rengeneration control.
		if (curHealthPoints < maxHealthPoints) {
			curHealthPoints += Time.deltaTime * healthRegenFactor;
		}
		if (curHealthPoints > maxHealthPoints) {
			curHealthPoints = maxHealthPoints;
		}

		// Energy Rengeneration control.
		if (curEnergyPoints < maxEnergyPoints) {
			curEnergyPoints += Time.deltaTime * energyRegenFactor;
		}
		if (curEnergyPoints > maxEnergyPoints) {
			curEnergyPoints = maxEnergyPoints;
		}

		if (moderatedlyDamagedSprite != null) {
			// The objective here is to load a different sprite for a weakened wall.
			// Requires to know more about scripting tiles.
		}
		
		if (moderatedlyDamagedSprite != null) {
			// The objective here is to load a different sprite for a heavily weakened wall.
			// Requires to know more about scripting tiles.
		}
	}

	// Get, Set, Find...
	// Current Health.
	public void SetHealth(float newValue) {
		curHealthPoints = newValue;
	}
	public float GetHealth() {
		return curHealthPoints;
	}

	// Current Energy.
	public void SetEnergy(float newValue) {
		curEnergyPoints = newValue;
	}
	public float GetEnergy() {
		return curEnergyPoints;
	}

	// Current Debuffs.
	public void AddDebuff(Debuff newDebuff) {
		currentDebuffs.Add(newDebuff);
	}
	
	public bool FindDebuff(Debuff target) {

        if (currentDebuffs != null)
        {
            foreach (var d in currentDebuffs)
            {
                if (d == target)
                    return true;

            }
        }
        else
            return false;
		return false;
	}

	public void RemoveDebuff(Debuff target) {
		currentDebuffs.Remove(target);
	}
}
