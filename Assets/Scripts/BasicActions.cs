﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicActions : MonoBehaviour {

    Rigidbody2D rb;

    // Use this for initialization
    void Awake () {
        rb = GetComponent<Rigidbody2D>();
    }
	
	// Update is called once per frame
	void Update () {
    }



    public void Move(Vector2 direction, float speed)
    {
        rb.velocity = direction * speed * Time.deltaTime;
    }
}
