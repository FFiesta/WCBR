﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInput : MonoBehaviour {
    
	public int x;
	public int y;
	public bool action1;
	public bool action2;
	
	// Update is called once per frame
	void Update () {
		if(Input.GetButton("Horizontal")) {
			if(Input.GetAxisRaw("Horizontal") > 0)
				x = 1;
			else if (Input.GetAxisRaw("Horizontal") < 0)
				x = -1;
		} else 
			x = 0;

			
		if(Input.GetButton("Vertical")) {
			if(Input.GetAxisRaw("Vertical") > 0)
				y = 1;
			else if (Input.GetAxisRaw("Vertical") < 0)
				y = -1;
		} else 
			y = 0;

		action1 = Input.GetButton("Fire1");
		action2 = Input.GetButton("Fire2");
		
	}
    
}
