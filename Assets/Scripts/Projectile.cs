﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour {
	
	public Vector2 projectileSpeed;
	public GameObject hitSprite;

	Rigidbody2D rb;

	void Awake() {
		rb = GetComponent<Rigidbody2D>();
	}
	
	void Update () {
		rb.velocity = projectileSpeed;


	}

	void OnCollisionEnter2D(Collision2D other)
	{
		var hitSprite = Instantiate(this.hitSprite);
		hitSprite.transform.position = this.transform.position;
		Destroy(gameObject);
	}
}
