﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wizard : MonoBehaviour {

    PlayerInput pi;
    Stats stats;
    BasicActions bActions;
    Animator a;

    public SpecialAbility[] startingAbilityBook;

    /*
	
	PlayerActions ps;
	public float moveSpeed;
	public Transform weaponPosition;
    

	
	public Projectile[] spellGO;
	public float[] spellCastSpeed;

	public List<Spell> currentSpellbook;
	public List<float> spellCurrentCooldown;
    */
    void Awake () {

        stats = GetComponent<Stats>();
        pi = GetComponent<PlayerInput>();
        bActions = GetComponent<BasicActions>();
        a = GetComponent<Animator>();

        /*foreach (var item in startingSpellbook)
		{
			currentSpellbook.Add(item);
		}*/
    }
    /*
	
    */
    void Update () {

        bActions.Move(new Vector2(pi.x, pi.y), stats.moveSpeed);

        if (pi.x != 0 || pi.y != 0 ) {
            a.SetBool("Walking", true);
            a.SetFloat("x", pi.x);
            a.SetFloat("y", pi.y);
        } else {
            a.SetBool("Walking", false);
        }


        /*
		for (var i = 0 ; i < spellCurrentCooldown.Count; i++){
			if (spellCurrentCooldown[i] > 0)
				spellCurrentCooldown[i] -= Time.deltaTime;
			else 
				RestoreSpeedAnimation();
		}
        */
    }

    /*

	public void CastSpell (Spell item) { 
		if (spellCurrentCooldown[0] <= 0) {
			a.SetTrigger("Attack");
			Debug.Log("Attacking");
			Projectile fireball = Instantiate(currentSpellbook[0]);
			fireball.transform.position = ps.weaponPosition.position;
			spellCurrentCooldown[0] = spellCastSpeed;
			SetCastSpeedAnimation(spellCastSpeed);
		}
		else 
			a.ResetTrigger("Attack");
				
    }

    public void RestoreSpeedAnimation()
    {
        a.speed = 1;
    }
    */
}
